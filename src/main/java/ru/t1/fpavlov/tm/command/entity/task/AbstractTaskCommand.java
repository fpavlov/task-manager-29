package ru.t1.fpavlov.tm.command.entity.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.service.ITaskService;
import ru.t1.fpavlov.tm.command.entity.AbstractEntityCommand;
import ru.t1.fpavlov.tm.model.Task;
import ru.t1.fpavlov.tm.util.TerminalUtil;

import java.util.List;

/**
 * Created by fpavlov on 08.12.2021.
 */
public abstract class AbstractTaskCommand extends AbstractEntityCommand {

    @NotNull
    protected final ITaskService getTaskService() {
        return this.getServiceLocator().getTaskService();
    }

    @Nullable
    protected final Task findById() {
        System.out.println("Enter task id");
        @NotNull final String itemId = TerminalUtil.nextLine();
        @NotNull final String userId = this.getUserId();
        return this.getTaskService().findById(userId, itemId);
    }

    @Nullable
    protected final Task findByIndex() {
        System.out.println("Enter task index");
        @NotNull final Integer itemIndex = TerminalUtil.nextInteger();
        @NotNull final String userId = this.getUserId();
        return this.getTaskService().findByIndex(userId, itemIndex - 1);
    }

    protected final void renderEntities(
            @NotNull final List<Task> entities,
            @Nullable final String listName
    ) {
        if (listName != null) System.out.println(listName);
        for (int i = 0; i < entities.size(); i++) {
            System.out.format("|%2d%s%n", i + 1, entities.get(i));
        }
    }

}
