package ru.t1.fpavlov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.model.User;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Change password";

    @NotNull
    public static final String NAME = "change-password";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final User user = this.getAuthService().getUser();
        @Nullable final String login = user.getLogin();
        @NotNull final String oldPassword = this.input("Old password:");
        this.getAuthService().login(login, oldPassword);
        @NotNull final String newPassword = this.input("New password:");
        @NotNull final String passwordConfirmation = this.input("New password again:");
        if (!oldPassword.equals(newPassword) && newPassword.equals(passwordConfirmation)) {
            this.getServiceLocator().getUserService().setPassword(user.getId(), newPassword);
        }
    }

}
