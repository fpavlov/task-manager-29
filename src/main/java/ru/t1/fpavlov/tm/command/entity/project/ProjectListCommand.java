package ru.t1.fpavlov.tm.command.entity.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.model.Project;

import java.util.List;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "List project";

    @NotNull
    public static final String NAME = "project-list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final Sort sort = this.askEntitySort();
        @NotNull final String userId = this.getUserId();
        @NotNull final List<Project> projects = this.getProjectService().findAll(userId, sort);
        this.renderEntities(projects, "Projects:");
    }

}
