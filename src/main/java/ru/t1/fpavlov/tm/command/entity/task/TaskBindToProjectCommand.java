package ru.t1.fpavlov.tm.command.entity.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.util.TerminalUtil;

/**
 * Created by fpavlov on 09.12.2021.
 */
public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Bind task to project";

    @NotNull
    public static final String NAME = "bind-task-project";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("Enter the project id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter the task id:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final String userId = this.getUserId();
        this.getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
    }

}
