package ru.t1.fpavlov.tm.exception.user;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 15.01.2022.
 */
public class PermissionException extends AbstractUserException {

    @NotNull
    public PermissionException() {
        super("Error! Permission denied");
    }

}
