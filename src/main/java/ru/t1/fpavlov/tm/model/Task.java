package ru.t1.fpavlov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.model.IWBS;
import ru.t1.fpavlov.tm.enumerated.Status;

import java.util.Date;

/*
 * Created by fpavlov on 10.10.2021.
 */
@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String projectId = null;

    @NotNull
    private Date created = new Date();

    @NotNull
    public Task(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public Task(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return String.format(" |%40s |%10s |%20s |%20s |%20s |%40s |",
                this.getId(),
                this.created,
                this.name,
                this.description,
                Status.toName(this.status),
                this.projectId);
    }

}
