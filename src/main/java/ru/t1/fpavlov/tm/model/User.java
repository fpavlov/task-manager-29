package ru.t1.fpavlov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.enumerated.Role;

/**
 * Created by fpavlov on 19.12.2021.
 */
@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractModel {

    private static final long serialVersionUID = 1;

    @NotNull
    public static final String LOGIN_FIELD_NAME = "login";

    @NotNull
    public static final String EMAIL_FIELD_NAME = "email";

    @Nullable
    private String login;

    @Nullable
    private String email;

    @Nullable
    private String passwordHash;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private Boolean isLocked = false;

    @NotNull
    private Role role = Role.USER;

    @NotNull
    public User(@Nullable final String login, @Nullable final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    @NotNull
    public User(
            @Nullable final String login,
            @Nullable final String passwordHash,
            @Nullable final String email
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    @NotNull
    public User(
            @Nullable final String login,
            @Nullable final String passwordHash,
            @Nullable final Role role
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    @NotNull
    @Override
    public String toString() {
        return String.format(" |%40s |%10s |%10s |%10s |%10s |%10s |%20s |",
                this.getId(),
                this.login,
                this.firstName,
                this.lastName,
                this.middleName,
                this.email,
                this.role.getDisplayName());
    }

}
