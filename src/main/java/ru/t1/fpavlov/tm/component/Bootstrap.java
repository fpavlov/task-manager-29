package ru.t1.fpavlov.tm.component;


import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.fpavlov.tm.api.repository.ICommandRepository;
import ru.t1.fpavlov.tm.api.repository.IProjectRepository;
import ru.t1.fpavlov.tm.api.repository.ITaskRepository;
import ru.t1.fpavlov.tm.api.repository.IUserRepository;
import ru.t1.fpavlov.tm.api.service.*;
import ru.t1.fpavlov.tm.command.AbstractCommand;
import ru.t1.fpavlov.tm.command.data.AbstractDataCommand;
import ru.t1.fpavlov.tm.command.data.DataBase64LoadCommand;
import ru.t1.fpavlov.tm.command.data.DataBinaryLoadCommand;
import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.exception.system.ArgumentIncorrectException;
import ru.t1.fpavlov.tm.exception.system.CommandIncorrectException;
import ru.t1.fpavlov.tm.repository.CommandRepository;
import ru.t1.fpavlov.tm.repository.ProjectRepository;
import ru.t1.fpavlov.tm.repository.TaskRepository;
import ru.t1.fpavlov.tm.repository.UserRepository;
import ru.t1.fpavlov.tm.service.*;
import ru.t1.fpavlov.tm.util.SystemUtil;
import ru.t1.fpavlov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

/*
 * Created by fpavlov on 06.10.2021.
 */
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMAND = "ru.t1.fpavlov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(
            projectRepository,
            taskRepository
    );

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new ProperyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(
            propertyService,
            userRepository,
            projectRepository,
            taskRepository
    );

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMAND);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            this.registry(clazz);
        }
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        this.registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        this.commandService.add(command);
    }

    public void listenerCommand(@Nullable final String commandName, final boolean checkRoles) {
        @Nullable final AbstractCommand command = this.getCommandService().getCommandByName(commandName);
        if (command == null) throw new CommandIncorrectException();
        if (checkRoles) authService.checkRoles(command.getRoles());
        command.execute();
    }

    private void listenerArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand command = this.getCommandService().getCommandByArgument(argument);
        if (command == null) throw new ArgumentIncorrectException();
        command.execute();
        quite();
    }

    private void initLogger() {
        this.loggerService.info("**WELCOME TO TASK MANAGER**");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("TASK-MANAGER IS SHUTTING DOWN");
            }
        });
    }

    public void run(@NotNull final String[] args) {
        if (areArgumentsAvailable(args)) {
            try {
                listenerArgument(args[0]);
                this.loggerService.command(args[0]);
                return;
            } catch (Exception e) {
                this.loggerService.error(e);
                return;
            }
        }

        interactiveCommandProcessing();
    }

    private void interactiveCommandProcessing() {
        this.initDemoData();
        this.initLogger();
        this.initPID();
        this.initData();
        this.initBackup();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("-- Please enter a command --");
                @Nullable final String param = TerminalUtil.nextLine();
                listenerCommand(param, true);
                System.out.println("Ok");
                this.loggerService.command(param);
            } catch (Exception e) {
                this.loggerService.error(e);
            }
        }
    }

    private static boolean areArgumentsAvailable(@Nullable final String[] args) {
        return (args != null && args.length > 0);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initData() {
        final boolean checkBinary = Files.exists(Paths.get(AbstractDataCommand.FILE_BINARY));
        if (checkBinary) this.listenerCommand(DataBinaryLoadCommand.NAME, false);
        if (checkBinary) return;
        final boolean checkBase64 = Files.exists(Paths.get(AbstractDataCommand.FILE_BASE64));
        if (checkBase64) this.listenerCommand(DataBase64LoadCommand.NAME, false);
    }

    private void initBackup() {
        this.backup.init();
    }

    private static void quite() {
        System.exit(0);
    }

    private void initDemoData() {
        this.projectService.create("Test project 4", "Test project 4");
        this.projectService.create("Test project 1", "Test project 1");
        this.projectService.create("Test project 3", "Test project 3");
        this.projectService.create("Test project 2", "Test project 2");
        this.projectService.create("Test project 5", "Test project 5");

        this.taskService.create("Test task 10", "Test task 10");
        this.taskService.create("Test task 9", "Test task 9");
        this.taskService.create("Test task 3", "Test task 3");
        this.taskService.create("Test task 13", "Test task 13");
        this.taskService.create("Test task 7", "Test task 7");
        this.taskService.create("Test task 5", "Test task 5");
        this.taskService.create("Test task 14", "Test task 14");
        this.taskService.create("Test task 2", "Test task 2");
        this.taskService.create("Test task 6", "Test task 6");
        this.taskService.create("Test task 8", "Test task 8");
        this.taskService.create("Test task 4", "Test task 4");
        this.taskService.create("Test task 12", "Test task 12");
        this.taskService.create("Test task 1", "Test task 1");
        this.taskService.create("Test task 15", "Test task 15");
        this.taskService.create("Test task 11", "Test task 11");

        this.userService.create("test1", "123", "user1@user.com");
        this.userService.create("test2", "123", "user2@user.com");
        this.userService.create("admin", "321", Role.ADMIB);
    }

}
