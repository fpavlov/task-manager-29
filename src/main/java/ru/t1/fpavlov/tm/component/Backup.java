package ru.t1.fpavlov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.command.data.AbstractDataCommand;
import ru.t1.fpavlov.tm.command.data.DataBackupLoadCommand;
import ru.t1.fpavlov.tm.command.data.DataBackupSaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by fpavlov on 06.02.2022.
 */
public final class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() {
        this.load();
        this.start();
    }

    public void save() {
        this.bootstrap.listenerCommand(DataBackupSaveCommand.NAME, false);
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.listenerCommand(DataBackupLoadCommand.NAME, false);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            Thread.sleep(3000);
            this.save();
        }
    }

}
