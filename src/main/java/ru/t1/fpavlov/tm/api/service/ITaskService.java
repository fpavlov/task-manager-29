package ru.t1.fpavlov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Task;

import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(
            @Nullable final String userId,
            @Nullable final String name
    );

    @NotNull
    Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    );

    @NotNull
    List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    );

    @NotNull
    List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final Sort sort
    );

    @NotNull
    Task update(
            @Nullable final String userId,
            @Nullable final Task task,
            @Nullable final String name,
            @Nullable final String description
    );

    @NotNull
    Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

    @NotNull
    Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    );

    @NotNull
    Task changeStatus(
            @Nullable final String userId,
            @Nullable final Task task,
            @Nullable final Status status
    );

    @NotNull
    Task changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    );

    @NotNull
    Task changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    );

}
