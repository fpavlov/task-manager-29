package ru.t1.fpavlov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

/**
 * Created by fpavlov on 13.01.2022.
 */
public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(@Nullable final String userId);

    @NotNull
    List<M> findAll(@Nullable final String userId);

    @NotNull
    List<M> findAll(@Nullable final String userId, @Nullable final Comparator comparator);

    @NotNull
    List<M> findAll(@Nullable final String userId, @Nullable final Sort sort);

    boolean existsById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    M findById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    M findByIndex(@Nullable final String userId, @Nullable final Integer index);

    int getSize(@Nullable final String userId);

    @Nullable
    M removeById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    M removeByIndex(@Nullable final String userId, @Nullable final Integer index);

    @Nullable
    M add(@Nullable final String userId, @Nullable final M item);

    @Nullable
    M remove(@Nullable final String userId, @Nullable final M item);

}
