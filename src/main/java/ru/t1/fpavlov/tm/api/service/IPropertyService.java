package ru.t1.fpavlov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.api.component.ISaltProvider;

/**
 * Created by fpavlov on 29.01.2022.
 */
public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

}
